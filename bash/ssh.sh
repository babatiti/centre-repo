#!/usr/bin/env bash
#num=$(who | sort -uk1,1 | wc -l)
count=$(who | sort --key=1,1 --unique | wc --lines)
date +"Today %d %B, there are $num users logged in onto the system"
if [ $count -gt 0 ]; then echo "\nWarning!"
fi
